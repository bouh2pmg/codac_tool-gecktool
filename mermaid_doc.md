```mermaid
graph TD
main((Gagner du temps)) --> App{Apprenants}

App-->|Evaluation par un pédago/assistant|note
App-->|Evaluation par un pair|note
App-->|S'auto-évaluer|note

evaluation --> note(Etablir une note sur différents langages/frameworks)

note --> |Exemple|exemple(8/10 front, 4/10algo, 6/10back)

exemple --> |Choix de la compétence à up|sujets(Ressort des exercices/sujets)
sujets --> semantique

main --> semantique(Analyse et semantique des projets)

semantique --> exosPi(Exos piscines)
semantique --> exosSu(Sujet)

tech((Caractéristiques techniques))

tech --> interface(Interface de connexion)
interface --> pedago(Accès admin - pédagos/assistants)
interface --> apprenants(Accès user - apprenants)

tech --> bilan(Réponde à un bilan de compétences)
bilan --> qTech(Questions techniques)
qTech --> ch(Choix de la techno)
bilan --> qSavoir(Questions de savoir-être)
tech --> bdc(Résulats BDC)

bdc --> grap(Graphiques en étoiles pour les grands poles des technos)
```